/* eslint-disable react/react-in-jsx-scope */
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import React, { lazy, Suspense } from 'react';

const PokemonList = lazy(() => import('../pages/PokemonList'));
const PokemonDetail = lazy(() => import('../pages/PokemonDetail'));
const PokemonMine = lazy(() => import('../pages/PokemonMine'));

const Routing = () => (
    <Router>
        <Suspense fallback={<div>Loading ...</div>}>
            <Switch>
                <Route exact path="/" component={PokemonList}/>
                <Route path="/detail/:id" component={PokemonDetail} />
                <Route path="/my-pokemon" component={PokemonMine} />
            </Switch>
        </Suspense>
    </Router>
);

export default Routing;