import axios from 'axios';
import _ from 'lodash';

export const FETCH_POKEMON_LIST    = 'GET_LIST_ALL';
export const FETCH_POKEMON_DETAIL  = 'GET_DETAIL';
export const FETCH_POKEMON_SPECIES = 'GET_SPECIES';
export const FETCH_POKEMON_REQUEST = 'REQUEST';
export const SAVE_POKEMON          = 'SAVE_POKEMON';
export const REMOVE_POKEMON        = 'REMOVE_POKEMON';
export const FETCH_MY_POKEMON      = 'FETCH_MY_POKEMON';
export const FETCH_POKEMON_MOVE    = 'FETCH_POKEMON_MOVE';

export function getList(params) {
    return function(dispatch) {
        let url = params.nextUrl ? params.nextUrl : 'https://pokeapi.co/api/v2/pokemon/';
        return axios.get(url)
        .then(function(resp) {

            dispatch({
                type: FETCH_POKEMON_LIST,
                payload: [...params.data, ...resp.data.results],
                nextUrl: resp.data.next,
            });
        }).catch(function(error) {
            console.log(error);
        });
    };
}

export function getDetail(id) {
    return dispatch =>  {
        dispatch(requestProcess());
        return axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
        .then(async function(resp) {
            await dispatch(getSpecies(resp.data.species.url));
            dispatch({
                type: FETCH_POKEMON_DETAIL,
                payload: resp.data,
                id,
            });
        }).catch(function(error) {
            console.log(error);
        });
    };
}

export function getSpecies(url) {
    return function(dispatch) {
        dispatch(requestProcess());
        return axios.get(url)
        .then(function(resp) {
            dispatch({
                type: FETCH_POKEMON_SPECIES,
                payload: resp.data,
            });
        }).catch(function(error) {
            console.log(error);
        });
    };
}

export function getMoves(url) {
    return function(dispatch) {
        // dispatch(requestProcess());
        return axios.get(url)
        .then(function(resp) {
            dispatch({
                type   : FETCH_POKEMON_MOVE,
                payload: resp.data,
            });
        }).catch(function(error) {
            console.log(error);
        });
    };
}

export function requestProcess() {
    return { type: FETCH_POKEMON_REQUEST };
}

export function savePokemon(dataDetail) {
    let dataPokemon = JSON.parse(localStorage.getItem('myPokemon'));
    if (dataPokemon !== null) {
        dataPokemon.push(dataDetail);
    } else {
        dataPokemon = [dataDetail];
    }
    localStorage.setItem('myPokemon', JSON.stringify(dataPokemon));
    return { type: SAVE_POKEMON, status: 'ok' }
}

export function removePokemon(name) {
    let dataPokemon = JSON.parse(localStorage.getItem('myPokemon'));
    if (dataPokemon !== null) {
        _.remove(dataPokemon, function(n) {
            return n.name === name;
          });
    }
    localStorage.setItem('myPokemon', JSON.stringify(dataPokemon));
    return { type: REMOVE_POKEMON, status: 'ok' }
}

export function fetchMyPokemon() {
    let dataPokemon = JSON.parse(localStorage.getItem('myPokemon'));
    if (!dataPokemon) {
        dataPokemon = [];
    }
    return { type: FETCH_MY_POKEMON, payload: dataPokemon }
}
