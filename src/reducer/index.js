import { FETCH_POKEMON_LIST, FETCH_POKEMON_DETAIL, FETCH_POKEMON_MOVE, FETCH_POKEMON_SPECIES, FETCH_POKEMON_REQUEST, SAVE_POKEMON, FETCH_MY_POKEMON, REMOVE_POKEMON } from '../action/Action';
const initialState = {
    data: [],
    nextUrl: null,
    hasMore: true,
    dataDetail: {},
    id: null,
    loading: false,
    status: false,
    speciesDetail: {},
    moveDetail: {},
    myPokemonData: [],
}

export default (state = initialState, action) => {
    switch(action.type) {
        case FETCH_POKEMON_LIST:
            return {
                ...state,
                data: action.payload,
                count: state.count + 3,
                hasMore: action.nextUrl ? true : false,
                nextUrl: action.nextUrl,
            };
        case FETCH_POKEMON_DETAIL:
            return {
                ...state,
                dataDetail: action.payload,
                id: action.id,
                loading: false,
            };
        case FETCH_POKEMON_SPECIES:
            return {
                ...state,
                speciesDetail: action.payload,
                loading: false,
            };
        case FETCH_POKEMON_MOVE:
            return {
                ...state,
                moveDetail: action.payload,
                loading: false,
            };
        case FETCH_POKEMON_REQUEST:
            return {
                loading: true,
            };
        case SAVE_POKEMON:
            return {
                status: action.status,
            };
        case REMOVE_POKEMON:
            return {
                status: action.status,
            };
        case FETCH_MY_POKEMON:
            return {
                myPokemonData: action.payload,
            };
        default:
            return state;
    }
}