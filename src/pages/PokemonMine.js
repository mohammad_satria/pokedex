import React, { Component } from 'react';
import { Grid, Tabs, Tab, AppBar, IconButton } from '@material-ui/core';
import ListIcon from '@material-ui/icons/List';
import MyPokemonIcon from '@material-ui/icons/Face';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { fetchMyPokemon, removePokemon } from '../action/Action'
import {Button} from 'primereact/button';
import OutdoorGrillIcon from '@material-ui/icons/OutdoorGrill';
import Img from 'react-cool-img';
import BackIcon from '@material-ui/icons/ArrowBack';
class PokemonMine extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data: [],
        nextUrl: null,
        hasMore: true,

      }
    }

    async componentDidMount() {
        const { dispatch } = this.props;
        document.body.className = 'white';
        await dispatch(fetchMyPokemon());
    }


    remove = async (name) => {
        const { dispatch } = this.props;
        await dispatch(removePokemon(name));
        dispatch(fetchMyPokemon());
    }

    clickDetail = (id) => {
      this.props.history.push(`detail/${id}`);
    }
    
    renderPokeList = () => {
        const data = this.props.data;
        const dataResult = [];
        if (this.props.data) {
          // eslint-disable-next-line array-callback-return
          data.map((val, key) => {
            let color = 'blue-color';
            if ((val.id - 1) % 5 === 0) {
              color = 'cyan-color';
            } else if ((val.id - 1) % 3 === 0) {
              color= 'yellow-color';
            }
            dataResult.push(
              <Grid key={key} item lg={2} md={3} sm={4} xs={6}>
                   <div className={`box ${color}`}>
                      <h3>{val.name}</h3>
                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={12}>
                          <div onClick={() => this.clickDetail(val.id)}>
                            <Img 
                              src={`https://pokeres.bastionbot.org/images/pokemon/${val.id}.png`} 
                              width={100}
                              height={100}
                              debounce={500}
                              alt={val.name} 
                            />
                          </div>
                        </Grid>
                        <Grid item xs={12}>
                            <Button label="Remove" onClick={() => this.remove(val.name)} className="p-button-danger"/>
                        </Grid>
                      </Grid>
                    </div>
              </Grid>
            );
          });
        }
        return dataResult;
    }

    backToList = () => {
      this.props.history.push('/');
    }

    render() {
        const { data } = this.props;
        return (
          <React.Fragment>
            <div className="detail-header">
              <AppBar position="fixed" style={{ height: '50px' }} color="default">
                <Grid container>
                  <Grid item xs={2} style={{ textAlign: 'left!important' }}>
                    <IconButton onClick={this.backToList} aria-label="Back To List">
                      <BackIcon style={{ fontSize: '39px', marginTop: '-6px' }} />
                    </IconButton>
                  </Grid>
                  <Grid style={{ textAlign: 'center' }} item xs={8}>
                    <span>My Pokemon</span>
                  </Grid>
                </Grid>
              </AppBar>
            </div>

            <Grid container justify="center" alignItems="center">
                {
                    data && data.length > 0 ? 
                        this.renderPokeList()
                     : (
                        <div className="note">
                            <OutdoorGrillIcon />
                            <center>
                                No Data Found
                            </center>
                        </div>
                    )
                }
            </Grid>
              <br/>
              <br/>
              <br/>
            <AppBar position="fixed" style={{ bottom: '0',  top: 'auto' }} color="default">
                <Tabs
                    value={1}
                    indicatorColor="primary"
                    centered
                >
                    <Tab style={{ width: '50%' }} label="List" component={Link} to='/' icon={<ListIcon />} />
                    <Tab style={{ width: '50%' }} label="My Pokemon" component={Link} to='/my-pokemon' icon={<MyPokemonIcon />} />
                </Tabs>
            </AppBar>

          </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
  return {
    data: state.myPokemonData,
  };
}
export default connect(mapStateToProps)(PokemonMine)
