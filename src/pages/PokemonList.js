import React, { Component } from 'react';
import { Grid, Tabs, Tab, AppBar, Chip } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroller';
import ListIcon from '@material-ui/icons/List';
import MyPokemonIcon from '@material-ui/icons/Face';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { getList } from '../action/Action'
import _ from 'lodash';
import Img from 'react-cool-img';
import Skeleton from '@yisheng90/react-loading';
class PokemonList extends Component {
    isOffline = false;

    async componentDidMount() {
      const { nextUrl, dispatch } = this.props;
      const params = {
        nextUrl: nextUrl,
        data: [],
      }
      await dispatch(getList(params));
      document.body.className = 'white-color';
      this.isOffline = true;
    }

    clickDetail = (id) => {
      this.props.history.push(`detail/${id}`);
    }
    
    renderPokeList = () => {
        const data = this.props.data;
        const dataResult = [];
        if (this.props.data) {
          const myPokemon = JSON.parse(localStorage.getItem('myPokemon'));
          // eslint-disable-next-line array-callback-return
          data.map((val, key) => {
            const id = key + 1;
            const total = myPokemon !== null ? _.filter(myPokemon, { id: String(id) }).length : 0;
            let color = 'blue-color';
            if (key % 5 === 0) {
              color = 'cyan-color';
            } else if (key % 3 === 0) {
              color= 'yellow-color';
            }
            const pokeName = val.name.replace(/^\w/, c => c.toUpperCase());
            dataResult.push(
              <Grid key={key} item lg={2} md={3} sm={4} xs={6}>
                    <div onClick={() => this.clickDetail(id)} className={`box ${color}`}>
                      <h3 className="list-poke-name">{pokeName}</h3>
                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                          <Chip className="chip" label={`Owned: ${total}`} color="primary" />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <Img 
                            src={`https://pokeres.bastionbot.org/images/pokemon/${id}.png`} 
                            width={100}
                            height={100}
                            debounce={500}
                            alt={val.name} 
                          />
                        </Grid>
                      </Grid>
                    </div>
              </Grid>
            );
          });
        }
        return dataResult;
    }
    handleLoadMore = async (page) => {
      const { data, nextUrl, dispatch } = this.props;
      const params = {
        nextUrl: nextUrl,
        data: data,
      }
      await dispatch(getList(params));
    }

    renderLoadingCard = () => {
      const result = [];
      for (let i = 0; i <= 5; i++) {
        result.push(
            <Grid key={i} item lg={2} md={3} sm={4} xs={6}>
              <div className={`box`}>
                <center>
                  <Skeleton height="20px"/>
                </center>
                <br/>
                <Grid container>
                  <Grid item xs={12} sm={6}>
                    <center>
                      <Skeleton width={80} height={20}/>
                    </center>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <center>
                      <Skeleton width={90} height={100}/>

                    </center>
                  </Grid>
                </Grid>
              </div>
            </Grid>
        );
      }

      return result;
    }

    render() {
        return (
          <React.Fragment>
            <div className="detail-header">
              <AppBar position="fixed" style={{ height: '50px' }} color="default">
                <Grid container>
                  <Grid style={{ textAlign: 'center' }} item xs={12}>
                    <span>Pokemon List</span>
                  </Grid>
                </Grid>
              </AppBar>
            </div>

            <InfiniteScroll
              pageStart={0}
              loadMore={this.handleLoadMore}
              hasMore={this.props.hasMore}
              loader={<Grid key={Math.random()} container justify="center" alignItems="center">{this.renderLoadingCard()}</Grid>}
              >
                <Grid container justify="center" alignItems="center">
                  {this.renderPokeList()}
                </Grid>
              </InfiniteScroll>
              <AppBar position="fixed" style={{ bottom: '0',  top: 'auto' }} color="default">
                <Tabs
                  value={0}
                  indicatorColor="primary"
                  centered
                >
                  <Tab style={{ width: '50%' }} label="List" component={Link} to='/' icon={<ListIcon />} />
                  <Tab style={{ width: '50%' }} label="My Pokemon" component={Link} to='/my-pokemon' icon={<MyPokemonIcon />} />
                </Tabs>
            </AppBar>

          </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
  return {
    data: state.data,
    nextUrl: state.nextUrl,
    hasMore: state.hasMore,
  };
}
export default connect(mapStateToProps)(PokemonList)
