/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import { Grid, AppBar, IconButton, Chip, Tabs, Tab } from '@material-ui/core';
import { connect } from 'react-redux'
import { getDetail, savePokemon, getMoves } from '../action/Action'
import _ from 'lodash';
import BackIcon from '@material-ui/icons/ArrowBack';
import LinearProgress from '@material-ui/core/LinearProgress';
import Img from 'react-cool-img';
import PokeImg from '../assets/images/pokeball.png';
import {Accordion, AccordionTab} from 'primereact/accordion';
import { observable } from "mobx"
import { observer } from "mobx-react"
import Loading from './Loading';
const Pokeball = () => {
  return (
    <div>
        <Img src={PokeImg} /> 
    </div>
  )
}
@observer
class PokemonDetail extends Component {
  @observable abilities = null;
  @observable eggGroups = null;
  @observable actived = 0;
  @observable description = null;
  @observable pokeName = null;
  @observable color = null;
  @observable moveDesc = null;
  @observable power = 0;
  @observable accuracy = 0;
  @observable pp = 0;
  @observable loadingMoves = false;

    constructor(props) {
      super(props);
      this.onTabOpen = this.onTabOpen.bind(this);
    }
    
    async componentDidMount() {
        const { id } = this.props.match.params;
        const { dispatch } = this.props;
        await dispatch(getDetail(id));

        if (this.props.dataDetail !== undefined) {
          const types = _.map(this.props.dataDetail.types, 'type.name');
          const abilities = _.map(this.props.dataDetail.abilities, 'ability.name');
          const eggGroups = _.map(this.props.speciesDetail.egg_groups, 'name');
  
          const filterDesc = _.filter(this.props.speciesDetail.flavor_text_entries, (val, key) => {
            return val.language.name === 'en'
          });
  
          const key = id - 1;
          let color = 'blue-color';
          if (key % 5 === 0) {
            color = 'cyan-color';
          } else if (key % 3 === 0) {
            color= 'yellow-color';
          }
          document.body.className = color;
        
          this.pokeName = this.props.dataDetail.name.replace(/^\w/, c => c.toUpperCase());
          this.types = types;
          this.color = color;
          this.eggGroups =  _.join(eggGroups, ', ');
          this.abilities = _.join(abilities, ', ');
          this.description = filterDesc[0].flavor_text;
        } else {
          alert('Data not found');
          // setTimeout(() => {
              this.props.history.push('/');
          // }, 2000);
        }

    }

    catchPokemon = () => {
      const probab = Math.random();
      if (probab >= 0.5) {
        const name = prompt('Please enter name');
        this.save(name);
      } else {
        alert('Please try again to catch this pokemon');
      }
    }

    save = (name) => {
      const { dispatch, id } = this.props;
      if (!name) {
        alert('Please input name');
      } else {
        const data = {
          id,
          name,
        }
        dispatch(savePokemon(data));
        this.props.history.push('/my-pokemon');
      }
    }

    backToList = () => {
      this.props.history.push('/');
    }

    renderType = () => {
      const result = [];
      if (this.types) {
        // eslint-disable-next-line array-callback-return
        this.types.map((val, key) => {
          result.push(
            <Chip 
              className="chip-type"
              color="secondary" 
              key={key} 
              label={val.replace(/^\w/, c => c.toUpperCase())}
            /> 
          );
        });
      }
      return result;
    }

    onTabOpen = async (e) => {
      const { dataDetail, dispatch } = this.props;
      this.loadingMoves = true;
      const url = dataDetail.moves[e.index].move.url;
      this.moveDesc = '';
      this.accuracy = null;
      this.pp = null;
      this.power = null;
      await dispatch(getMoves(url));
      const filterDesc = _.find(this.props.moveDetail.flavor_text_entries, (val, key) => {
        return val.language.name === 'en' && val.version_group.name === 'ultra-sun-ultra-moon'
      });
      this.moveDesc = filterDesc.flavor_text;
      this.accuracy = this.props.moveDetail.accuracy ? this.props.moveDetail.accuracy : 'N/A';
      this.power = this.props.moveDetail.power ? this.props.moveDetail.power : 'N/A';
      this.pp = this.props.moveDetail.pp ? this.props.moveDetail.pp : 'N/A';
      this.loadingMoves = false;
    }

    renderMoves = () => {
      const { dataDetail } = this.props;
      const result = [];
      // eslint-disable-next-line array-callback-return
      dataDetail.moves.map((val, key) => {
          result.push(
            <AccordionTab id={val.move.url} key={val.move.name} header={val.move.name.replace(/^\w/, c => c.toUpperCase())}>
              {
                !this.loadingMoves && this.props.moveDetail ? (
                  <div className="align-center">
                        <div className="box-profile">
                            <div className="subtitle">Power</div>
                            <div className="subtitle-value">{this.power}</div>
                            <div className="subtitle-value">
                              <LinearProgress color="primary" variant="determinate" value={this.props.moveDetail.power === 'N/A' ? 0 : this.props.moveDetail.power} />
                          </div>
                        </div>

                        <div className="box-profile">
                          <div className="subtitle">Accuracy</div>
                          <div className="subtitle-value">{this.accuracy}</div>
                          <div className="subtitle-value">
                            <LinearProgress color="secondary" variant="determinate" value={this.props.moveDetail.accuracy} />
                        </div>
                      </div>

                      <div className="box-profile">
                        <div className="subtitle">PP</div>
                        <div className="subtitle-value">{this.pp}</div>
                        <div className="subtitle-value">
                          <LinearProgress color="primary" variant="determinate" value={this.props.moveDetail.pp} />
                       </div>
                    </div>
                  </div>
                ) : <div>Loading ...</div>
              }
              <p>
                {this.moveDesc}
              </p>
            </AccordionTab>
          );
      });
      return (
        <Accordion className="moves-detail" onTabOpen={this.onTabOpen}>
          { result}
        </Accordion>
      );
    }

    renderProfile = () => {
      const { dataDetail, speciesDetail } = this.props;
      const count = Object.keys(dataDetail).length; 
      if (count > 0) {
          return (
            <div style={{ textAlign: 'center' }}>
              <div className="box-profile">
                  <div className="subtitle">Weight </div>
                  <div className="subtitle-value">{dataDetail.weight} Kg</div>
              </div>
              
              <div className="box-profile">
                  <div className="subtitle">Height</div>
                  <div className="subtitle-value">{dataDetail.height} m</div>
              </div>

              <div className="box-profile">
                  <div className="subtitle">Species</div>
                  <div className="subtitle-value">{dataDetail.species.name}</div>
              </div>

              <div className="box-profile">
                  <div className="subtitle">Abilities</div>
                  <div className="subtitle-value">{this.abilities}</div>
              </div>
              <div className="box-profile">
                  <div className="subtitle">Catch Rate</div>
                  <div className="subtitle-value">{ `${speciesDetail.capture_rate} %`}</div>
              </div>
              <div className="box-profile">
                  <div className="subtitle">Egg Group</div>
                  <div className="subtitle-value">{this.eggGroups}</div>
              </div>
              <div className="box-profile">
                  <div className="subtitle">Generation</div>
                  <div className="subtitle-value">{speciesDetail.generation.name}</div>
              </div>
              <div className="box-profile">
                  <div className="subtitle">Habitat</div>
                  <div className="subtitle-value">{speciesDetail.habitat ? speciesDetail.habitat.name : 'N/A'}</div>
              </div>
              <div className="box-profile">
                  <div className="subtitle">Hatch Counter</div>
                  <div className="subtitle-value">{speciesDetail.hatch_counter}</div>
              </div>
            </div>
        )
      }
    }

    renderBaseStats = () => {
      const { dataDetail } = this.props;
      const count = Object.keys(dataDetail).length; 
      if (count > 0) {
          return (
            <div className="align-center">
              <div className="box-profile">
                  <div className="subtitle">Speed</div>
                  <div className="subtitle-value">{dataDetail.stats[0].base_stat}</div>
                  <div className="subtitle-value">
                    <LinearProgress color="secondary" variant="determinate" value={dataDetail.stats[0].base_stat} />
                 </div>
              </div>

              <div className="box-profile">
                  <div className="subtitle">Hp</div>
                  <div className="subtitle-value">{dataDetail.stats[5].base_stat}</div>
                  <div className="subtitle-value">
                    <LinearProgress color="primary" variant="determinate" value={dataDetail.stats[5].base_stat} />
                 </div>
              </div>

              <div className="box-profile">
                  <div className="subtitle">Attack</div>
                  <div className="subtitle-value">{dataDetail.stats[4].base_stat}</div>
                  <div className="subtitle-value">
                    <LinearProgress color="secondary" variant="determinate" value={dataDetail.stats[4].base_stat} />
                 </div>
              </div>

              <div className="box-profile">
                  <div className="subtitle">Defense</div>
                  <div className="subtitle-value">{dataDetail.stats[3].base_stat}</div>
                  <div className="subtitle-value">
                    <LinearProgress color="primary" variant="determinate" value={dataDetail.stats[3].base_stat} />
                 </div>
              </div>

              <div className="box-profile">
                  <div className="subtitle">Sp. Attack</div>
                  <div className="subtitle-value">{dataDetail.stats[2].base_stat}</div>
                  <div className="subtitle-value">
                    <LinearProgress color="secondary" variant="determinate" value={dataDetail.stats[2].base_stat} />
                 </div>
              </div>

              <div className="box-profile">
                  <div className="subtitle">Sp. Defense</div>
                  <div className="subtitle-value">{dataDetail.stats[1].base_stat}</div>
                  <div className="subtitle-value">
                    <LinearProgress color="primary" variant="determinate" value={dataDetail.stats[1].base_stat} />
                 </div>
              </div>
            </div>
        )
      }
    }

    handleChangeTab = (event, value) => {
      this.actived = value;
    }

    render() {
      const { id, dataDetail, loading } = this.props;
        return (
          <React.Fragment>
              {
                !loading && dataDetail ? (
                  <>
                    <AppBar className={this.color} position="static">
                      <Grid container>
                        <Grid item xs={2}>
                          <IconButton onClick={this.backToList} aria-label="Back To List">
                            <BackIcon className="back-icon" />
                          </IconButton>
                        </Grid>
                        <Grid className="align-center" item xs={8}>
                          <h2>Pokemon Detail</h2>
                        </Grid>
                      </Grid>
                    </AppBar>
                    <div className={`${this.color}`}>
                      <div className="box-detail">
                        <Grid container >
                          <Grid item xs={9}>
                            <h1 className="pokename">{this.pokeName}</h1>
                          </Grid>

                          <Grid item xs={3}>
                            <h1 className="align-right">#{dataDetail.id}</h1>  
                          </Grid>

                          <Grid item xs={12}>
                            <div className="img-detail">
                              <img 
                                src={`https://pokeres.bastionbot.org/images/pokemon/${id}.png`} 
                                width={300}
                                height={300}
                              />
                            </div> 
                          </Grid>

                          <Grid item xs={12}>
                            <div className="img-detail">
                                { this.renderType() }
                            </div>
                          </Grid>
                        </Grid>
                      </div>

                      <div className="box-detail-content">
                        <Tabs
                          indicatorColor="primary"
                          textColor="primary"
                          variant="scrollable"
                          scrollButtons="off"
                          value={this.actived}
                          onChange={this.handleChangeTab}
                        >
                          <Tab label="About" />
                          <Tab label="Base Stats"/>
                          <Tab label="Moves"/>
                        </Tabs>
                        <Grid container>
                          {
                            this.actived === 0 && (
                              <Grid item xs={12} lg={12}>
                                  <div className="description">
                                    {this.description}
                                  </div>
                                  { this.renderProfile() }
                              </Grid>
                            )
                          }

                          {
                            this.actived === 1 && (
                              <Grid item xs={12} lg={12}>
                                { this.renderBaseStats() }
                              </Grid>
                            )
                          }

                          {
                            this.actived === 2 && (
                              <Grid item xs={12} lg={12}>
                                  {this.renderMoves()}
                              </Grid>
                            )
                          }
                        </Grid>
                      </div>
                    </div>


                    <AppBar position="fixed" style={{ bottom: '0', height: '50px'  ,top: 'auto' }} className={this.color}>
                      <Grid container>
                        <Grid className="align-center" item xs={12}>
                            <IconButton onClick={this.catchPokemon} className="pokeball">
                              <Pokeball />
                            </IconButton>
                        </Grid>
                      </Grid>
                    </AppBar>
                  </>
                ) : <Loading />
              }
          </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
  return {
    dataDetail: state.dataDetail,
    id: state.id,
    loading: state.loading,
    speciesDetail: state.speciesDetail,
    moveDetail: state.moveDetail,
  };
}
export default connect(mapStateToProps)(PokemonDetail)
