/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import Loader from '../assets/images/loading.gif';
import Img from 'react-cool-img';

class Loading extends Component {
    render() {
        return (
            <div className="loading-container bg-transparant">
                <div className="loading-loader">
                    <Img 
                        src={Loader}
                    />
                    <h3>Please Wait ...</h3>
                </div>
            </div>
        )
    }
}

export default Loading;
